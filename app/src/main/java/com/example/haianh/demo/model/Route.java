package com.example.haianh.demo.model;

import android.view.View;

import java.util.ArrayList;

/**
 * Created by HaiAnh on 22/06/2017.
 */

public class Route {
    private ArrayList<Leg> legs;

    public Overview_polyline getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(Overview_polyline overview_polyline) {
        this.overview_polyline = overview_polyline;
    }

    private Overview_polyline overview_polyline;

    public ArrayList<Leg> getLegs() {
        return legs;
    }

    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }
}
