package com.example.haianh.demo.model;

/**
 * Created by HaiAnh on 26/06/2017.
 */

public class Overview_polyline {
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
