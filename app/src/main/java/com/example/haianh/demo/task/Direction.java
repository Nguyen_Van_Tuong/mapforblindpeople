package com.example.haianh.demo.task;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.example.haianh.demo.activity.LocationActivity;
import com.example.haianh.demo.model.GetDataFromGG;
import com.example.haianh.demo.model.GoogleObj;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by HaiAnh on 26/06/2017.
 */

public class Direction extends AsyncTask<String,Void,GoogleObj>{

    private LocationActivity activity;
    private GetDataFromGG getDATA;
    ProgressDialog progressDialog;
    public Direction(LocationActivity activity) {
        super();
        this.activity = activity;
    }



    @Override
    protected GoogleObj doInBackground(String... strings) {
        String origin = strings[0];
        String des =null;
        GoogleObj googleObj = new GoogleObj();
        try {
            des = URLEncoder.encode(strings[1],"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String aip = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + des + "&mode=walking&key=AIzaSyBAH4JcsqPzmd8Nl8i_2ikddNIXffiQO1U&language=vi";
        try {
            URL url = new URL(aip);
            InputStreamReader inputStreamReader = new InputStreamReader(url.openStream(), "utf-8");
            googleObj  = new Gson().fromJson(inputStreamReader, GoogleObj.class);
            return googleObj;
        } catch (Exception e) {
            Log.i(" ", "doInBackground: ");
        }
        return googleObj;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog= new ProgressDialog(activity);
        progressDialog.setProgressStyle(DialogInterface.BUTTON_POSITIVE);
        progressDialog.setMessage("Getting data from Sever!!");
        progressDialog.show();

    }

    @Override
    protected void onPostExecute(GoogleObj googleObj) {

        progressDialog.cancel();
        if(googleObj.getRoutes()!=null)
            getDATA.getData(googleObj);
    }
    public void DirectionData(GetDataFromGG getDATA) {
        this.getDATA = getDATA;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}