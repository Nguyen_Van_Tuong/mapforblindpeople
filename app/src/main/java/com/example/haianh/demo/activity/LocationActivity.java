package com.example.haianh.demo.activity;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Location;
import com.example.haianh.demo.R;
import com.example.haianh.demo.task.GetAddressTask;
import com.example.haianh.demo.task.Scanning;
import com.google.android.gms.location.LocationListener;
import android.location.LocationManager;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.example.haianh.demo.model.Compass;
import com.example.haianh.demo.model.GetDataFromGG;
import com.example.haianh.demo.model.GoogleObj;
import com.example.haianh.demo.task.Direction;
import com.example.haianh.demo.model.IOnDeviceRotationListener;
import com.example.haianh.demo.model.Route;
import com.example.haianh.demo.model.Step;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import org.jsoup.Jsoup;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Nguyễn Văn Tường - Soncamedia
 * */
public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final int REQ_CODE_SPEECH_INPUT = 100;
    private static final long UPDATE_INTERVAL = 5000;
    private static final long FASTEST_INTERVAL = 5000;
    private static final int REQUEST_LOCATION_PERMISSION = 100;

    ImageButton btnMyLocation, btnScan, btnNavigate, btnDirection;
    EditText txtDes;
    String destination = "";

    private GoogleMap mMap;
    private Compass mRotationSensor;
    public String mLocationStr;
    TextToSpeech t2s;
    LatLng curLocation;
    MarkerOptions markerDes = new MarkerOptions();
    MarkerOptions markerCur = new MarkerOptions();
    ArrayList<Route> myRoutes = new ArrayList<>();
    ArrayList<String> infrontRouts = new ArrayList<>();
    ArrayList<String> behindRouts = new ArrayList<>();
    ProgressDialog progressDialog;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private SensorManager mSensorManager;
    private int mBearing;
    private boolean is1st = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        requestLocationPermissions();
        if (isPlayServicesAvailable()) {
            setUpLocationClientIfNeeded();
            buildLocationRequest();
        } else {
            Toast.makeText(LocationActivity.this, "Device does not support Google Play services", Toast.LENGTH_LONG).show();
        }

        addControls();
        addEvents();
    }

    /**
     *  Khởi tạo view
     * */
    private void addControls() {

        btnMyLocation = (ImageButton) findViewById(R.id.imgSpeak);
        btnScan = (ImageButton) findViewById(R.id.imgRecord);
        btnNavigate = (ImageButton) findViewById(R.id.imgDirection);
        btnDirection = (ImageButton) findViewById(R.id.imgSearch);

        txtDes = (EditText) findViewById(R.id.txtDes);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mRotationSensor = new Compass(mSensorManager);

        prepareTTS();

    }
    //TTS
    private void prepareTTS() {

        TextToSpeech.OnInitListener temp;
        temp = new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t2s.setLanguage(Locale.getDefault());
                }
            }
        };
        t2s = new TextToSpeech(getApplicationContext(), temp);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
//        mMap.setMyLocationEnabled(true);
   }

    /**
     *  Gán sự kiện
     * */
    private void addEvents() {

        btnMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGpsOn()) {
                    Toast.makeText(LocationActivity.this, "GPS is OFF",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                GetAddressTask getAddressTask = new GetAddressTask(LocationActivity.this);
                getAddressTask.execute(String.valueOf(curLocation.latitude), String.valueOf(curLocation.longitude));

            }
        });

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Scanning scanning = new Scanning(LocationActivity.this, curLocation, mBearing, progressDialog);
                scanning.execute();
            }
        });

        mRotationSensor.setOnDeviceRotationListener(new IOnDeviceRotationListener() {
            @Override
            public void onRotate(int bearing) {

                if ((Math.abs(mBearing - bearing) >= 45) || is1st) {
                    is1st = false;
                    mBearing = bearing;
                    btnScan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Scanning scanning = new Scanning(LocationActivity.this, curLocation, mBearing, progressDialog);
                            scanning.execute();
                        }
                    });

                }
            }
        });

        btnDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtDes.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "MUỐN ĐI ĐÂU THÌ NHẬP VÀO >.< !", Toast.LENGTH_LONG).show();
                } else startDirection(txtDes.getText().toString());
            }
        });

        btnNavigate.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                clearMap();
                return true;
            }
        });

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myRoutes.size() != 0) {
                    for (Step x : myRoutes.get(0).getLegs().get(0).getSteps()
                            ) {
                        double la = x.getStart_location().getLat();
                        double lo = x.getStart_location().getLng();
                        String str = x.getHtml_instructions();
                        str = Jsoup.parse(str).text();
                        if ((Math.abs(lo - curLocation.longitude) <= 0.00025)
                                && Math.abs(la - curLocation.latitude) <= 0.00025) {
                            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
                            t2s.speak(str, TextToSpeech.QUEUE_ADD, null);
                        }
                    }
                } else {
                    t2s.speak(" Bạn muốn đi đâu ", TextToSpeech.QUEUE_ADD, null);
                    while (t2s.isSpeaking()) {
                    }
                    startVoiceActivity();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRotationSensor.registerSensor();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        mRotationSensor.unregisterSensor();
    }

    @Override
    protected void onDestroy() {
        mRotationSensor.unregisterSensor();
        if (mGoogleApiClient != null
                && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
            mGoogleApiClient = null;
        }
        super.onDestroy();
    }

    /**
     * Chỉ đường bằng giọng nói
     **/
    private void startVoiceActivity(){
       Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
       intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
       intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
         try {
           startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
       } catch (ActivityNotFoundException a) {}
   }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT : {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    txtDes.setText(text);
                        communicate(text);
                }
                break;
            }
        }
    }
    //Xác nhận điểm đến
    private void communicate(String text) {

        if (text.equals("ok")) {
            startDirection(destination);
        }
        else {
            destination = text;
            t2s.speak("Có phải bạn muốn đến " + destination, TextToSpeech.QUEUE_ADD, null);
            Toast.makeText(getApplicationContext(),"Có phải bạn muốn đến " + destination,Toast.LENGTH_LONG).show();
            while (t2s.isSpeaking()){}
            if(!t2s.isSpeaking())
                startVoiceActivity();
        }
    }

    //Thoát khỏi chế độ tìm đường
    private void clearMap() {
        myRoutes.clear();
        mMap.clear();
        txtDes.setText("");
    }

    /**
     *  Tìm giao cắt phía trước và phía sau
     *  */
    public void ibRouts(String[] routs) {

        for (int i =0; i<routs.length/2;i++){
            if(!mLocationStr.contains(routs[i])&& !infrontRouts.contains(routs[i]))
                infrontRouts.add(routs[i]);
        }
        for (int i =routs.length/2; i<routs.length;i++){
            if(!mLocationStr.contains(routs[i]) && !behindRouts.contains(routs[i]))
                behindRouts.add(routs[i]);
        }
        scanningResult();

    }

    private void scanningResult() {
        if (infrontRouts.size() != 0){
            for ( String x : infrontRouts) {
                Direction direction = new Direction(this);
                direction.execute(mLastLocation.getLatitude()+","+mLastLocation.getLongitude(),"đường" + x);
                direction.DirectionData(new GetDataFromGG() {
                    @Override
                    public void getData(GoogleObj googleObj) {
                        if(googleObj.getRoutes().size() > 0){
                            if (googleObj.getRoutes().get(0).getLegs().get(0).getSteps().size()<=2){

                                String str = "Phía trước : " + googleObj.getRoutes().get(0).getLegs().get(0).getSteps().get(0).getHtml_instructions() + " cách vị trí hiện tại khoảng : "
                                        +googleObj.getRoutes().get(0).getLegs().get(0).getSteps().get(0).getDistance().getText();
                                str=Jsoup.parse(str).text();
                                t2s.speak(str,TextToSpeech.QUEUE_ADD,null);
                                Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
                            }
                            else  {Toast.makeText(getApplicationContext(), "Phía trước không đi được !", Toast.LENGTH_LONG).show();
                                t2s.speak("Phía trước không đi được ",TextToSpeech.QUEUE_ADD,null);
                            }
                        }
                    }
                });
            }

        }else {Toast.makeText(getApplicationContext(), "Phía trước không có giao cắt !", Toast.LENGTH_LONG).show();
            t2s.speak("Phía trước không có giao cắt nào !",TextToSpeech.QUEUE_ADD,null);
        }

        if (behindRouts.size() != 0){
            for ( String x : behindRouts) {
                Direction direction = new Direction(this);
                direction.execute(curLocation.latitude+","+curLocation.longitude,x);
                direction.DirectionData(new GetDataFromGG() {
                    @Override
                    public void getData(GoogleObj googleObj) {
                        if(googleObj.getRoutes().size() > 0){
                            if (googleObj.getRoutes().get(0).getLegs().get(0).getSteps().size()<=2){

                                String str = "Phía sau : " + googleObj.getRoutes().get(0).getLegs().get(0).getSteps().get(0).getHtml_instructions() + " cách vị trí hiện tại khoảng : "
                                        +googleObj.getRoutes().get(0).getLegs().get(0).getSteps().get(0).getDistance().getText();
                                str=Jsoup.parse(str).text();
                                t2s.speak(str,TextToSpeech.QUEUE_ADD,null);
                                Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "Phía sau có thể có chướng ngại vật !", Toast.LENGTH_LONG).show();

                            }
                        }
                    }
                });
            }

        }


        else{  Toast.makeText(getApplicationContext(), "Phía sau có thể có chướng ngại vật !", Toast.LENGTH_LONG).show();
            t2s.speak("Phía sau có thể có chướng ngại vật!",TextToSpeech.QUEUE_ADD,null);
        }
        infrontRouts.clear();
        behindRouts.clear();
    }

    /**
     * Tìm đường từ textinput
     * */
    private void startDirection(String des) {
        String origin = curLocation.latitude + "," + curLocation.longitude;
        mMap.clear();
        Direction direction = new Direction(this);
        direction.DirectionData(new GetDataFromGG() {
            @Override
            public void getData(GoogleObj googleObj) {
                if (googleObj.getRoutes().size() != 0) {

                    myRoutes = googleObj.getRoutes();

                    double la = myRoutes.get(0).getLegs().get(0).getEnd_location().getLat();
                    double lo = myRoutes.get(0).getLegs().get(0).getEnd_location().getLng();

                    markerDes = new MarkerOptions().position(new LatLng(la, lo)).title("Destination").icon(getMarkerIcon("#ff99cc00"));
                    markerDes.visible(true);
                    mMap.addMarker(markerDes);

                    List<LatLng> list = decodePolyLine(myRoutes.get(0).getOverview_polyline().getPoints());
                    Path(list);

                    mMap.addMarker(markerCur.position(curLocation).snippet(mLocationStr).title("Bạn đang ở đây")).showInfoWindow();
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(la, lo)));
                }
                else
                    Toast.makeText(getApplication(), "Đĩa bay đang chờ bạn ( ͡° ͜ʖ ͡°)", Toast.LENGTH_SHORT).show();
            }
        });

        direction.execute(origin, des);
    }

    //Hiển thị path trên map
    private List<LatLng> decodePolyLine(final String s) {
        int len = s.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = s.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = s.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(lat / 100000d, lng / 100000d));
        }

        return decoded;
    }
    private void Path (List<LatLng> list){
        List<Polyline> path = new ArrayList<>();
        PolylineOptions polylineOptions = new PolylineOptions().
            geodesic(true).
            color(Color.BLUE).
            width(12);
            for (int i = 0; i < list.size(); i++)
            polylineOptions.add(list.get(i));
        path.add(mMap.addPolyline(polylineOptions));

    }
    private BitmapDescriptor getMarkerIcon(String s) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(s), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    //Tự động cập nhật vị trí hiện tại
    protected void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest,  LocationActivity.this);
    }

    //Dừng chế độ cập nhật vị trí hiện tại
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,  LocationActivity.this);
    }

    private void buildLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

    }

    private void setUpLocationClientIfNeeded() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    //Request quyền sử dụng định vị
    private void requestLocationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }
        if ( ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
    }

    //Kiểm tra ggPlaySV
    private boolean isPlayServicesAvailable() {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
                == ConnectionResult.SUCCESS;
    }

    //Kiểm tra GPS
    private boolean isGpsOn() {
        LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    //Hiển thị vị trí hiện tại trên map
    public void locationResult() {

        markerCur.title("Bạn đang ở đây nầy!");
        markerCur.snippet(mLocationStr);
        markerCur.position(curLocation);
        mMap.addMarker(markerCur).showInfoWindow();
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLocation, 16));
        }

        Toast.makeText(getApplicationContext(), mLocationStr, Toast.LENGTH_LONG).show();
        t2s.speak(mLocationStr, TextToSpeech.QUEUE_ADD, null);

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        curLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (lastLocation != null) {
            curLocation = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
            mLastLocation = lastLocation;
        }

        startLocationUpdates();

        if (!isGpsOn()) {
            Toast.makeText(LocationActivity.this, "GPS is OFF",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            GetAddressTask getAddressTask = new GetAddressTask(LocationActivity.this);
            getAddressTask.execute(String.valueOf(curLocation.latitude), String.valueOf(curLocation.longitude));
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    return;
                } else {
                    requestLocationPermissions();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
