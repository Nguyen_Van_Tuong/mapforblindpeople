package com.example.haianh.demo.model;

/**
 * Created by HaiAnh on 22/06/2017.
 */

public class End_location {
    private Double lat;
    private Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
