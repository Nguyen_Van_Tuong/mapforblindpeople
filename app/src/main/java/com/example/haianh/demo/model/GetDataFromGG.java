package com.example.haianh.demo.model;

/**
 * Created by HaiAnh on 28/06/2017.
 */

public interface GetDataFromGG {
    void getData(GoogleObj googleObj);
}
