package com.example.haianh.demo.model;

/**
 * Created by HaiAnh on 27/06/2017.
 */

public interface IOnDeviceRotationListener {
    void onRotate(int bearing);
}
