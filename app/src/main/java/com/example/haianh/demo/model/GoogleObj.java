package com.example.haianh.demo.model;

import java.util.ArrayList;


/**
 * Created by HaiAnh on 22/06/2017.
 */

public class GoogleObj {
    public GoogleObj() {
    }

    public ArrayList<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<Route> routes) {
        this.routes = routes;
    }

    private ArrayList<Route> routes;
}
